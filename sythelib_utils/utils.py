#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes
__all__ = [
    "LazyLoadBitmapFromString",
    "namespace_unpacker",
    "find",
    "get",
    "move_mouse_to_edge",
    "in_combat",
    "get_right_click_text",
    "get_health",
    "get_run_energy",
    "click_random_bitmap_point",
    "click_random_square_point",
    "get_top_left_text",
    "check_top_left_text",
    "contains",
    "click_random_circle_point",
    "random_click_mouse",
    "bitmap_picker",
    "color_picker",
    "get_chat"
]

import math
import random
import typing
from operator import attrgetter
from types import SimpleNamespace

from pylibsythe import *

from sythelib_utils.custom_mouse import random_move_mouse
from sythelib_utils.lazy_load_bitmap_from_string import LazyLoadBitmapFromString

T = typing.TypeVar("T")


#  A bitmap of the top right of right clicking
_top_right_click = LazyLoadBitmapFromString(
    "0000r00aODLRx4sF+o`-Q(3}9^|Sy007d{p004jl0w4e|%>aa^oG`?xokO;~0l*&@az!{$kitauv;Y7A"
)


def scrape_game():
    scrape_rect(0, 0, 515, 337)


def namespace_unpacker(
    namespace: SimpleNamespace, *attrs: str
) -> typing.List[typing.Any]:
    """
    Unpacks a :obj:`~types.SimpleNamespace` into a list of the attributes specified, in the
    order you passed them into the function.

    This function is useful if you need to unpack a namespace for use in another helper
    function.

    Args:
        namespace (:obj:`~types.SimpleNamespace`): :obj:`~types.SimpleNamespace` to get the attributes for.
        *attrs (:obj:`str`): Attributes to get.

    Returns:
        List[ :obj:`str` ]: Attributes retrieved.
    """
    return [getattr(namespace, attr) for attr in attrs]


def find(
    predicate: typing.Callable[[T], bool], seq: typing.Sequence[T]
) -> typing.Optional[T]:
    """
    A helper to return the first element found in the sequence
    that meets the predicate. For example:

    .. code-block:: python

        option = sythelib_utils.find(lambda m: m.text == "EatSalmon", options)

    would find the first :class:`SimpleNamespace` whose text is 'EatSalmon' and return it
    If an entry is not found, then ``None`` is returned
    This is different from :func:`py:filter` due to the fact it stops the moment it finds
    a valid entry

    :param predicate: typing.Callable[[T], bool]
        A function that returns a boolean-like result
    :param seq: typing.Sequence[T]
        The iterable to search through
    :return: T
        A list of strings from top to bottom of the found text

    """

    for element in seq:
        if predicate(element):
            return element
    return None


def get(iterable: typing.Iterable[T], **attrs: typing.Any) -> typing.Optional[T]:
    """
    A helper that returns the first element in the iterable that meets
    all the traits passed in ``attrs``. This is an alternative for
    :obj:`sythelib_utils.find`. For example:

    .. code-block:: python

        option = sythelib_utils.get(options, text="EatSalmon")

    would find the first :class:`SimpleNamespace` whose text is 'EatSalmon' and return it.
    When multiple attributes are specified, they are checked using
    logical AND, not logical OR. Meaning they have to meet every
    attribute passed in and not one of them.
    To have a nested attribute search (i.e. search by ``x.y``) then
    pass in ``x__y`` as the keyword argument.
    If nothing is found that matches the attributes passed, then
    ``None`` is returned.

    :param iterable: typing.Iterable[T]
        The iterable to search through
    :param attrs: typing.Any
        The attributes to look for in the iterable
    :return: T
        A list of strings from top to bottom of the found text

    """

    # global -> local
    _all = all
    attrget = attrgetter

    # Special case the single element call
    if len(attrs) == 1:
        k, v = attrs.popitem()
        pred = attrget(k.replace("__", "."))
        for elem in iterable:
            if pred(elem) == v:
                return elem
        return None

    converted = [
        (attrget(attr.replace("__", ".")), value) for attr, value in attrs.items()
    ]

    for elem in iterable:
        if _all(pred(elem) == value for pred, value in converted):
            return elem
    return None


def move_mouse_to_edge(teleport: bool = False) -> None:
    """
    Moves the mouse to a random edge. Useful to make it seem like you're focusing on another window

    :param teleport: bool
        Whether to teleport the mouse or move human-like. Defaults to human-like
    :return: None
    """
    w, h = get_width(), get_height()
    x_rand, y_rand = random.randint(0, w), random.randint(0, h)
    x, y = random.choice([(x_rand, 0), (0, y_rand), (x_rand, h), (w, y_rand)])

    if teleport:
        move_mouse(x, y)
    else:
        random_move_mouse(x, y)


def in_combat() -> bool:
    """
    Checks the top left combat plugin to see if you're in combat or not

    :return: bool
        True if you're in combat, False if not
    """
    return find_color(
        0x0A8633, 5, 3, 140, 66
    )  # looks for opponents green health if you're in combat


def get_right_click_text(x: int, y: int, font: int) -> typing.List[SimpleNamespace]:
    """
    Gets the output text from right clicking

    :param x: int
        x coordinate to right click
    :param y: int
        y coordinate to right click
    :param font: int
        The font to use when searching (should be font1())
    :return: typing.List[SimpleNamespace]
        A list of strings from top to bottom of the found text
    """

    random_click_mouse(x, y, 2)
    sleep(20)
    scrape()

    top_right = find_bitmap(_top_right_click())
    right, top = top_right.x, top_right.y

    # calculating the right click box dimensions
    bottom_left = find_text_using_font(font, "Cancel")
    left, bottom = bottom_left.x - bottom_left.width, bottom_left.y + bottom_left.height

    options = []
    last_top = top
    while last_top < bottom:  # this searches for the text line by line
        if n := ocr_using_font(font, left, last_top, right, bottom):
            last_top = n.y + n.height
            options.append(n)
        else:
            return options
    return options


def get_health(font: int) -> int:
    """
    Gets your current health

    :param font: int
        The font to use when searching (should be font3())
    :return: int
        Your current health which should be an integer between 10 and 99
    """
    scrape_rect(518, 50, 20, 20)  # health area
    return int(ocr_using_font(font, 515, 50, 544, 70).text)


def get_run_energy(font: int) -> int:
    """
    Gets your current run energy

    :param font: int
        The font to use when searching (should be font3())
    :return: int
        Your current run energy which should be an integer between 0 and 100
    """
    scrape_rect(518, 114, 85, 20)  # run area
    return int(ocr_using_font(font, 518, 114, 554, 137).text)


def get_special(font: int) -> int:
    """
    Gets your special attack percentage

    :param font: int
        The font to use when searching (should be font3())
    :return: int
        Your current Special attack percentage which should be an integer between 0 and 100
    """
    scrape_rect(550, 143, 574, 162)  # Special Attack Area
    return int(ocr_using_font(font, 552, 146, 572, 158).text)


def click_random_bitmap_point(
    bitmap: int, mouse_button: int = 1, tolerance: int = 0,
) -> bool:
    """
    Searches for a bitmap on the screen and clicks a random point within it if found.

    :param bitmap: int
        an integer of the bitmap to look for
    :param mouse_button: int
        which mouse button to press
    :param tolerance: int
        the tolerance to search for the bitmap with
    :return: bool
        True if the bitmap was found, False if not
    """
    scrape()
    if not (coordinates := find_bitmap(bitmap, color_tolerance=tolerance)):
        return False

    click_random_square_point(
        coordinates.x,
        coordinates.y,
        get_bitmap_width(bitmap),
        get_bitmap_height(bitmap),
        mouse_button,
    )
    return True


def click_random_square_point(
    x: int, y: int, width: int, height: int, mouse_button: int = 1
) -> None:
    """
    Clicks in a random square point. Calls on click_random_circle_point and focus on the center of the square for more
    human-like movements

    :param x: int
        the x coordinate top left point
    :param y: int
        the y coordinate top left point
    :param width: int
        the width of the square
    :param height: int
        the height of the square
    :param mouse_button: int
        the mouse button number to click
    :return: None
    """
    center_x = x + (width // 2)
    center_y = y + (height // 2)
    click_random_circle_point(
        center_x, center_y, mouse_button, width // 2 if width < height else height // 2,
    )


def get_chat(font: int) -> typing.List[SimpleNamespace]:
    """
    Scrapes and returns messages in the chat

    :param font: int
        the font to search for (should be font2 if using defaults)
    :return: typing.Optional[SimpleNamespace]
        Either returns the SimpleNamespace if text was found or False if not
    """
    return [
        ocr_using_font(font, 10, 372 + 14 * i, 496, 388 + 14 * i + 1) for i in range(8)
    ]


def get_top_left_text(font: int) -> typing.Optional[SimpleNamespace]:
    """
    Scrapes and returns the top left of the screen to find text

    :param font: int
        the font to search for (should be font1 if using defaults)
    :return: typing.Optional[SimpleNamespace]
        Either returns the SimpleNamespace if text was found or False if not
    """
    sleep(20)
    scrape_rect(0, 0, 300, 100)
    return ocr_using_font(font, right=300, bottom=100)


def check_top_left_text(words: typing.Union[typing.List[str], str], font: int) -> bool:
    """
    Scrapes the text in the top left of the screen, and compares it to a list to see if there were any matches

    :param words: typing.Union[typing.List[str], str]
        The word(s) to search for
    :param font: int
        the font to search for (should be font1() if using defaults)
    :return: bool
        True if found words matched any in the list, else False
    """
    if isinstance(words, str):
        words = [words]

    if not (found_text := get_top_left_text(font)):
        return False
    return contains(found_text.text, words)


def contains(
    found_word: str, words_to_search: typing.Union[typing.List[str], str]
) -> bool:
    """
    Checks if a found_word is container in a list of words
    It removes spaces and lowercases all the words before comparing

    :param found_word: str
        the word to search for
    :param words_to_search: typing.Union[typing.List[str], str]
        word(s) to search
    :return: bool
        returns True if the word is found in the list, else False
    """
    if isinstance(words_to_search, str):
        words_to_search = [words_to_search]

    formatted_words = [word.replace(" ", "").casefold() for word in words_to_search]
    return any(word in found_word.casefold() for word in formatted_words)


def click_random_circle_point(
    x: int, y: int, mouse_button: int = 1, r: int = 10
) -> None:
    """
    Randomly clicks a point in a circle

    :param x: int
        x coordinate
    :param y: int
        y coordinate
    :param r: int
        radius of the circle in pixels
    :param mouse_button: int
        which mouse button to click between left click (default), right click, or middle button click
    :return: None
    """
    alpha = 2 * math.pi * random.random()  # creates a random angle
    r = r * math.sqrt(random.random())  # creates a random radius
    x = r * math.cos(alpha) + x  # x coordinate
    y = r * math.sin(alpha) + y  # y coordinate
    random_click_mouse(int(x), int(y), mouse_button)


def random_click_mouse(x: int, y: int, mouse_button: int = 1) -> None:
    """
    Will randomly click the mouse over a random interval
    It will take a random 5-20 millisecond break between pressing the button and releasing it

    :param x: int
        the x coordinate of where to click in pixels
    :param y: int
        the y coordinate of where to click in pixels
    :param mouse_button: int
        the mouse button to click
    :return: None
    """
    random_move_mouse(x, y)
    mouse_button_down(x, y, mouse_button)
    sleep(random.randint(10, 20))
    mouse_button_up(x, y, mouse_button)


def _get_byte(value: int, byte: int):
    """Converts an int to byte"""
    return (value >> (8 * byte)) & 0xFF


def bitmap_picker() -> None:
    """
    Invokes the bitmap_picker for testing

    :return: None
    """
    n = show_bitmap_picker()
    print(
        f"Starting Location - x: {n.x:,}, y: {n.y:,}\n"
        f"Ending Location - x: {n.x + n.width:,} y: {n.y + n.height:,}\n"
        f"Width: {n.width} Height: {n.height}\n"
        f"{n.x}, {n.y}, {n.x + n.width}, {n.y + n.height}\n"
        f"{n.bitmap}"
    )


def color_picker() -> None:
    """
    Invokes the color_picker for testing

    :return: None
    """
    n = show_color_picker()
    print(
        f"Location: x: {n.x:,}, y: {n.y:,}\nColor: {n.color}\n{n.x}, {n.y}, {n.color}"
    )
