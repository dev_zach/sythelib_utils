#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
__all__ = [
    "NotLoggedInError",
    "Animation",
    "Slot",
    "Equipment",
    "GameObjects",
    "Coordinates",
    "Verts",
    "Polys",
    "GroundItems",
    "Health",
    "Inventory",
    "Npcs",
    "Players",
    "Pos",
    "Skill",
    "Skills",
    "TileMinimap",
    "VarBit",
    "VarPlayer",
    "Widget"
]
import abc
from typing import AnyStr, Sequence, Type, TypeVar, List, Optional, Final, Dict

import attr
import ujson

T = TypeVar("T")


class NotLoggedInError(Exception):
    pass


# Optional, but means you can check if something is a
# parsed ID with isinstance(x, IDType) in the future.
class IDType(int):
    __slots__: Sequence[str] = ()


NOT_LOGGED_ERROR: Final[Dict] = {"error": "not logged in"}
NOT_FOUND_ERROR: Final[Dict] = {"error": "not found"}


@attr.s(slots=True)
class Base(abc.ABC):
    @classmethod
    def from_json(cls: Type[T], raw: AnyStr) -> T:
        raw_obj = ujson.loads(raw)
        assert isinstance(
            raw_obj, (dict, list)
        ), f"expected Union[dict, list], got {raw_obj.__class__} for deserializing {cls}"
        if raw_obj == NOT_LOGGED_ERROR:
            raise NotLoggedInError()
        if raw_obj == NOT_FOUND_ERROR:
            return None
        return cls.from_dict(raw_obj) if isinstance(raw_obj, dict) else list(map(cls.from_dict, raw_obj))

    @classmethod
    @abc.abstractmethod
    def from_dict(cls: Type[T], data: dict) -> T:
        ...

    def __getitem__(self, item):
        return getattr(self, item)


@attr.s(slots=True)
class Animation(Base):
    """An animation dataclass"""

    animation_id: IDType = attr.ib()
    """The animation id"""
    spot_animation_id: IDType = attr.ib()
    """The spot animation id"""
    pose_id: IDType = attr.ib()
    """The pose id"""
    idle_pose_id: IDType = attr.ib()
    """The idle pose id"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(
            animation_id=IDType(obj["animation"]),
            spot_animation_id=IDType(obj["spotAnimation"]),
            pose_id=IDType(obj["pose"]),
            idle_pose_id=IDType(obj["poseIdle"]),
        )


@attr.s(slots=True)
class Slot(Base):
    """An equipment slot dataclass"""

    id: IDType = attr.ib()
    """The slot id"""
    amount: IDType = attr.ib()
    """The amount of items in that slot"""
    name: str = attr.ib()
    """The name of the item"""
    slot: IDType = attr.ib()
    """Which slot the item is in from 0-27"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(
            id=IDType(obj["id"]),
            amount=IDType(obj["amount"]),
            name=obj["name"],
            slot=IDType(obj["slot"]),
        )


@attr.s(slots=True)
class Equipment(Base):
    """An equipment dataclass"""

    head: Slot = attr.ib()
    """The head slot"""
    cape: Slot = attr.ib()
    """The cape slot"""
    amulet: Slot = attr.ib()
    """The amulet slot"""
    weapon: Slot = attr.ib()
    """The weapon slot"""
    torso: Slot = attr.ib()
    """The torso slot"""
    shield: Slot = attr.ib()
    """The shield slot"""
    legs: Slot = attr.ib()
    """The legs slot"""
    gloves: Slot = attr.ib()
    """The gloves slot"""
    boots: Slot = attr.ib()
    """The boots slot"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(
            head=Slot.from_dict(obj["head"]),
            cape=Slot.from_dict(obj["cape"]),
            amulet=Slot.from_dict(obj["amulet"]),
            weapon=Slot.from_dict(obj["weapon"]),
            torso=Slot.from_dict(obj["torso"]),
            shield=Slot.from_dict(obj["shield"]),
            legs=Slot.from_dict(obj["legs"]),
            gloves=Slot.from_dict(obj["gloves"]),
            boots=Slot.from_dict(obj["boots"]),
        )


@attr.s(slots=True)
class Coordinates(Base):
    """Basic coordinate dataclass"""

    x: IDType = attr.ib()
    """x coordinate"""
    y: IDType = attr.ib()
    """y coordinate"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(x=IDType(obj["x"]), y=IDType(obj["y"]),)


@attr.s(slots=True)
class Verts(Base):
    """A verts dataclass which holds points of a polygon"""

    verts: List[List[IDType]] = attr.ib(factory=list)
    """The points of a polygon"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(verts=list(list(map(IDType, vert)) for vert in obj["verts"]))


@attr.s(slots=True)
class Polys(Base):
    """A dataclass for polygons"""

    polys: List[Verts] = attr.ib(factory=list)
    """The polygons"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(polys=[Verts.from_dict(vert) for vert in obj["polys"]],)


@attr.s(slots=True)
class GameObjects(Base):
    """A game object dataclass"""

    id: IDType = attr.ib()
    """The id of the object"""
    name: str = attr.ib()
    """The name of the object"""
    pos: Coordinates = attr.ib()
    """The coordinates of the object"""
    canvas: Optional[Polys] = attr.ib()
    """The clickbox canvas of the item"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(
            id=IDType(obj["id"]),
            name=obj["name"],
            pos=Coordinates.from_dict(obj["pos"]),
            canvas=Polys.from_dict(obj["canvas"]) if "canvas" in obj else None,
        )


@attr.s(slots=True)
class GroundItems(Base):
    """A dataclass for the ground items"""

    id: IDType = attr.ib()
    """The item id"""
    quantity: Optional[IDType] = attr.ib()
    """The quantity of the item"""
    name: str = attr.ib()
    """The name of the item"""
    pos: Coordinates = attr.ib()
    """The coordinates of the item"""
    canvas: Optional[Polys] = attr.ib()
    """The clickbox canvas of the item"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(
            id=IDType(obj["id"]),
            quantity=obj["quantity"] if "quantity" in obj else None,
            name=obj["name"],
            pos=Coordinates.from_dict(obj["pos"]),
            canvas=Polys.from_dict(obj["canvas"]) if "canvas" in obj else None,
        )


@attr.s(slots=True)
class Health(Base):
    """A Health dataclass"""

    health: IDType = attr.ib()
    """Your current hp"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(health=IDType(obj["health"]),)


@attr.s(slots=True)
class Inventory(Base):
    """A dataclass of an Inventory"""

    items: List[Slot] = attr.ib()
    """The items in your inventory"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(items=list(map(Slot.from_dict, obj["items"])),)


@attr.s(slots=True)
class Npcs(Base):
    """A dataclass of NPCs"""

    combat_level: IDType = attr.ib()
    """The combat level"""
    health_ratio: IDType = attr.ib()
    """The health ratio"""
    health_scale: IDType = attr.ib()
    """The health scale"""
    interacting: str = attr.ib()
    """Who the npc is interacting with"""
    pos: Coordinates = attr.ib()
    """The coordinates"""
    index: IDType = attr.ib()
    """The index"""
    animation: IDType = attr.ib()
    """The animation"""
    canvas: Optional[Polys] = attr.ib()
    """The clickable canvas of the npc"""
    name: Optional[str] = attr.ib()
    """The name"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(
            combat_level=IDType(obj["combatLvl"]),
            health_ratio=IDType(obj["healthRatio"]),
            health_scale=IDType(obj["healthScale"]),
            interacting=obj["interacting"],
            pos=Coordinates.from_dict(obj["pos"]),
            index=IDType(obj["index"]),
            animation=IDType(obj["animation"]) if "animation" in obj else None,
            canvas=Polys.from_dict(obj["canvas"]) if "canvas" in obj else None,
            name=obj["name"] if "name" in obj else None,
        )


@attr.s(slots=True)
class Players(Base):
    """The Players dataclass"""

    local_player: bool = attr.ib()
    """Whether it's a local player or not"""
    id: IDType = attr.ib()
    """The id"""
    pos: Coordinates = attr.ib()
    """The coordinates"""
    health_ratio: IDType = attr.ib()
    """The health ratio"""
    health_scale: IDType = attr.ib()
    """The health scale"""
    level: IDType = attr.ib()
    """The level"""
    interacting: str = attr.ib()
    """Who the player is interacting with"""
    interacting_id: IDType = attr.ib()
    """The interacting id"""
    overhead: IDType = attr.ib()
    """The overhead"""
    skulled: bool = attr.ib()
    """Whether the player is skulled"""
    equipment: Equipment = attr.ib()
    """The equipment the player is wearing"""
    canvas: Optional[Polys] = attr.ib()
    """The clickbox of the player"""
    name: Optional[str] = attr.ib()
    """The name"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(
            local_player=bool(obj["localPlayer"]),
            id=IDType(obj["id"]),
            pos=Coordinates.from_dict(obj["pos"]),
            health_ratio=IDType(obj["healthRatio"]),
            health_scale=IDType(obj["healthScale"]),
            level=IDType(obj["level"]),
            interacting=obj["interacting"],
            interacting_id=IDType(obj["interactingID"]),
            overhead=IDType(obj["overhead"]),
            skulled=bool(obj["skulled"]),
            equipment=Equipment.from_dict(obj["equipment"]),
            canvas=Polys.from_dict(obj["canvas"]) if "canvas" in obj else None,
            name=obj["name"] if "name" in obj else None,
        )


@attr.s(slots=True)
class Pos(Coordinates):
    """The position of the player"""
    x: IDType = attr.ib()
    """x coordinate"""
    y: IDType = attr.ib()
    """y coordinate"""
    z: IDType = attr.ib()
    """z coordinate"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(x=IDType(obj["x"]), y=IDType(obj["y"]), z=IDType(obj["z"]))


@attr.s(slots=True)
class Skill(Base):
    """The skill dataclass"""

    level: IDType = attr.ib()
    """The level"""
    boosted_level: IDType = attr.ib()
    """The boosted level"""
    xp: IDType = attr.ib()
    """The current xp"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(
            level=IDType(obj["level"]),
            boosted_level=IDType(obj["boostedLevel"]),
            xp=IDType(obj["xp"]),
        )


@attr.s(slots=True)
class Skills(Base):
    """A skills dataclass"""

    attack: Skill = attr.ib()
    """The attack level"""
    defense: Skill = attr.ib()
    """The defense level"""
    strength: Skill = attr.ib()
    """The strength level"""
    hitpoints: Skill = attr.ib()
    """The hitpoints level"""
    ranged: Skill = attr.ib()
    """The ranged level"""
    prayer: Skill = attr.ib()
    """The prayer level"""
    magic: Skill = attr.ib()
    """The magic level"""
    cooking: Skill = attr.ib()
    """The cooking level"""
    woodcutting: Skill = attr.ib()
    """The woodcutting level"""
    fletching: Skill = attr.ib()
    """The fletching level"""
    fishing: Skill = attr.ib()
    """The fishing level"""
    firemaking: Skill = attr.ib()
    """The firemaking level"""
    crafting: Skill = attr.ib()
    """The crafting level"""
    smithing: Skill = attr.ib()
    """The smithing level"""
    mining: Skill = attr.ib()
    """The mining level"""
    herblore: Skill = attr.ib()
    """The herblore level"""
    agility: Skill = attr.ib()
    """The agility level"""
    thieving: Skill = attr.ib()
    """The thieving level"""
    slayer: Skill = attr.ib()
    """The slayer level"""
    farming: Skill = attr.ib()
    """The slayer level"""
    runecrafting: Skill = attr.ib()
    """The slayer level"""
    hunter: Skill = attr.ib()
    """The slayer level"""
    construction: Skill = attr.ib()
    """The slayer level"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(
            attack=Skill.from_dict(obj["Attack"]),
            defense=Skill.from_dict(obj["Defense"]),
            strength=Skill.from_dict(obj["Strength"]),
            hitpoints=Skill.from_dict(obj["Hitpoints"]),
            ranged=Skill.from_dict(obj["Ranged"]),
            prayer=Skill.from_dict(obj["Attack"]),
            magic=Skill.from_dict(obj["Magic"]),
            cooking=Skill.from_dict(obj["Cooking"]),
            woodcutting=Skill.from_dict(obj["Woodcutting"]),
            fletching=Skill.from_dict(obj["Fletching"]),
            fishing=Skill.from_dict(obj["Fishing"]),
            firemaking=Skill.from_dict(obj["Firemaking"]),
            crafting=Skill.from_dict(obj["Crafting"]),
            smithing=Skill.from_dict(obj["Smithing"]),
            mining=Skill.from_dict(obj["Mining"]),
            herblore=Skill.from_dict(obj["Herblore"]),
            agility=Skill.from_dict(obj["Agility"]),
            thieving=Skill.from_dict(obj["Thieving"]),
            slayer=Skill.from_dict(obj["Slayer"]),
            farming=Skill.from_dict(obj["Farming"]),
            runecrafting=Skill.from_dict(obj["Runecrafting"]),
            hunter=Skill.from_dict(obj["Hunter"]),
            construction=Skill.from_dict(obj["Construction"]),
        )


@attr.s(slots=True)
class TileMinimap(Coordinates):
    """The tile scene of the player"""

    pass


@attr.s(slots=True)
class VarBit(Base):
    """A var bit dataclass"""

    value: IDType = attr.ib()
    """The int value result"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(value=IDType(obj["value"]),)


@attr.s(slots=True)
class VarPlayer(Base):
    """A var player dataclass"""

    value: IDType = attr.ib()
    """The value of the result"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(value=IDType(obj["value"]),)


@attr.s(slots=True)
class Widget(Base):
    """The Widget dataclass"""
    x: IDType = attr.ib()
    """The x coordinate"""
    y: IDType = attr.ib()
    """The y coordinate"""
    width: IDType = attr.ib()
    """The width of the rectangle"""
    height: IDType = attr.ib()
    """The height of the rectangle"""
    is_hidden: bool = attr.ib()
    """Whether the widget is hidden"""

    @classmethod
    def from_dict(cls: Type[T], obj: dict) -> T:
        return cls(
            x=IDType(obj["x"]),
            y=IDType(obj["y"]),
            width=IDType(obj["width"]),
            height=IDType(obj["height"]),
            is_hidden=bool(obj["isHidden"]),
        )
