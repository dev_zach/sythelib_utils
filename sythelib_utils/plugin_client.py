#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
__all__ = ["PluginClient"]

from enum import Enum
from typing import TypeVar, Optional, Dict, Any, List, Union, Iterable

import requests
from pylibsythe import sleep
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

from sythelib_utils import models

T = TypeVar("T")


def _make_dict_from_kwargs(**kwargs: Any) -> Dict[str, Any]:
    params = {}
    for key, value in kwargs.items():
        if value is not None:
            params.update({key: value})
    return params


class PluginClient:
    """
    The Plugin Client class to interact with the client. It prioritizes speed and organization - meaning
    it's bare bones without magic methods. It's able to grab info from the plugin API and display it
    in object form.
    """
    def __init__(self, port: int = 8081, max_retry: int = 5) -> None:
        self._session = self._create_session()
        self._port = port
        self._max_retry = max_retry

    @staticmethod
    def _create_session() -> requests.Session:
        s = requests.Session()
        retry = Retry(total=3, read=3, connect=3, backoff_factor=0.3, )
        adapter = HTTPAdapter(max_retries=retry)
        s.mount("http://", adapter)
        s.mount("https://", adapter)
        return s

    @staticmethod
    def _make_json_canvas_from_list(canvas: models.Polys) -> Dict[str, Any]:
        payload = {"polys": []}
        for poly in canvas.polys:
            payload["polys"].append({"verts": [[v[0], v[1]] for v in poly.verts]})
        return payload

    def _build_url_for(self, target: T, extra_route: str = "") -> str:
        return f"http://localhost:{self._port}/{target.__name__.lower()}{f'/{extra_route}' if extra_route else ''}"

    def _backoff(self, a: int, r: float) -> Iterable[int]:
        n = 0
        while n < self._max_retry:
            yield int(a * r ** n)
            n += 1

    def _make_request_for(self, target: T, *, parameters: Optional[Dict[str, Any]] = None, extra_route: str = "") -> Optional[Union[T, List[T]]]:
        if parameters is None:
            parameters = dict()

        for n in self._backoff(50, 2):
            try:
                with self._session.get(self._build_url_for(target, extra_route), params=parameters) as resp:
                    resp.raise_for_status()
                    return target.from_json(resp.content.decode("utf-8"))
            except models.NotLoggedInError:
                sleep(n)
                continue
        raise models.NotLoggedInError("You are not logged in.")

    def get_animation(self) -> Optional[models.Animation]:
        """
        Returns an instance of :obj:`~.models.Animation` class.

        :return: Optional[models.Animation]
        """
        return self._make_request_for(models.Animation)

    def get_equipment(self) -> Optional[models.Equipment]:
        """
        Returns an instance of :obj:`~.models.Equipment` class.

        :return: Optional[models.Equipment]
        """
        return self._make_request_for(models.Equipment)

    def get_game_objects(self, *, id: int = None, name: str = None) -> Optional[List[models.GameObjects]]:
        """
        Returns a list of of :obj:`~.models.GameObjects` class.

        :param id: Optional[int]
            the id of the game object to query for
        :param name: Optional[str]
            the name of the object to query for
        :return: Optional[List[models.GameObjects]]
        """
        params = _make_dict_from_kwargs(id=id, name=name)
        return self._make_request_for(models.GameObjects, parameters=params)

    def get_nearest_game_object(self, *, id: int = None, name: str = None, x: int = None, y: int = None, z: int = None) -> Optional[models.GameObjects]:
        """
        Returns an instance of :obj:`~.models.GameObjects` class.

        :param id: Optional[int]
            the id of the game object to query for
        :param name: Optional[str]
            the name of the game object to query for
        :param x: Optional[int]
            the x coordinate to query for
        :param y: Optional[int]
            the y coordinate to query for
        :param z: Optional[int]
            the y coordinate to query for
        :return: Optional[models.GameObjects]
        """
        params = _make_dict_from_kwargs(name=name, id=id, x=x, y=y, z=z)
        return self._make_request_for(models.GameObjects, parameters=params, extra_route="nearest")

    def get_ground_items(self, *, id: int = None, name: str = None) -> Optional[List[models.GroundItems]]:
        """
        Returns a list of of :obj:`~.models.GroundItems` class.

        :param id: Optional[int]
            the id of the game object to query for
        :param name: Optional[str]
            the name of the object to query for
        :return: Optional[List[models.GroundItems]]
        """
        params = _make_dict_from_kwargs(id=id, name=name)
        return self._make_request_for(models.GroundItems, parameters=params)

    def get_nearest_ground_item(self, *, id: int = None, name: str = None, x: int = None, y: int = None, z: int = None) -> Optional[models.GroundItems]:
        """
        Returns an instance of :obj:`~.models.GroundItems` class.

        :param id: Optional[int]
            the id of the game object to query for
        :param name: Optional[str]
            the name of the game object to query for
        :param x: Optional[int]
            the x coordinate to query for
        :param y: Optional[int]
            the y coordinate to query for
        :param z: Optional[int]
            the y coordinate to query for
        :return: Optional[models.GroundItems]
        """
        params = _make_dict_from_kwargs(name=name, id=id, x=x, y=y, z=z)
        return self._make_request_for(models.GroundItems, parameters=params, extra_route="nearest")

    def get_health(self) -> Optional[models.Health]:
        """
        Returns an instance of :obj:`~.models.Health` class.

        :return: Optional[models.Health]
        """
        return self._make_request_for(models.Health)

    def get_inventory(self) -> Optional[models.Inventory]:
        """
        Returns an instance of :obj:`~.models.Inventory` class.

        :return: Optional[models.Inventory]
        """
        return self._make_request_for(models.Inventory)

    def get_npcs(self, *, id: int = None, name: str = None) -> Optional[List[models.Npcs]]:
        """
        Returns a list of :obj:`~.models.Npcs` class.

        :param id: Optional[int]
            the id of the game object to query for
        :param name: Optional[str]
            the name of the game object to query for
        :return: Optional[List[models.Npcs]]
        """
        params = _make_dict_from_kwargs(id=id, name=name)
        return self._make_request_for(models.Npcs, parameters=params)

    def get_nearest_npc(self, *, id: int = None, name: str = None, x: int = None, y: int = None, z: int = None) -> Optional[models.Npcs]:
        """
        Returns an instance of :obj:`~.models.Npcs` class.

        :param id: Optional[int]
            the id of the game object to query for
        :param name: Optional[str]
            the name of the game object to query for
        :param x: Optional[int]
            the x coordinate to query for
        :param y: Optional[int]
            the y coordinate to query for
        :param z: Optional[int]
            the y coordinate to query for
        :return: Optional[models.Npcs]
        """
        params = _make_dict_from_kwargs(id=id, name=name, x=x, y=y, z=z)
        return self._make_request_for(models.Npcs, parameters=params, extra_route="nearest")

    def get_players(self) -> Optional[List[models.Players]]:
        """
        Returns an list of :obj:`~.models.Players` class.

        :return: Optional[List[models.Players]]
        """
        return self._make_request_for(models.Players)

    def get_position(self) -> Optional[models.Pos]:
        """
        Returns an list of :obj:`~.models.Pos` class.

        :return: Optional[models.Pos]
        """
        return self._make_request_for(models.Pos)

    def get_skills(self) -> Optional[models.Skills]:
        """
        Returns an list of :obj:`~.models.Skills` class.

        :return: Optional[models.Skills]
        """
        return self._make_request_for(models.Skills)

    def get_minimap_tile(self, *, x: int = None, y: int = None, z: int = None) -> Optional[models.TileMinimap]:
        """
        Returns an list of :obj:`~.models.TileMinimap` class.

        :param x: Optional[int]
            the x coordinate to query for
        :param y: Optional[int]
            the y coordinate to query for
        :param z: Optional[int]
            the y coordinate to query for
        :return: Optional[models.TileMinimap]
        """
        params = _make_dict_from_kwargs(x=x, y=y, z=z)
        return self._make_request_for(models.TileMinimap, parameters=params)

    def get_varbit(self, number: Union[int, Enum]) -> Optional[models.VarBit]:
        """
        Varbit values are useful to search for progress in the game inclusing quest completions etc.
        Returns an list of :obj:`~.models.VarBit` class.

        :param number: Union[int, Enum]
            The int or Enum of the varbit to query for
        :return: Optional[models.VarBit]
        """
        if isinstance(number, Enum):
            number = number.value

        params = _make_dict_from_kwargs(varbit=number)
        return self._make_request_for(models.VarBit, parameters=params)

    def get_varplayer(self, number: Union[int, Enum]) -> Optional[models.VarPlayer]:
        """
        Varplayer values are useful to search for status on the player including absorption, overload, poison, etc.
        Returns an list of :obj:`~.models.VarPlayer` class.

        :param number: Union[int, Enum]
            The int or Enum of the varplayer to query for
        :return: Optional[models.VarPlayer]
        """
        if isinstance(number, Enum):
            number = number.value

        params = _make_dict_from_kwargs(varplayer=number)
        return self._make_request_for(models.VarPlayer, parameters=params)

    def get_widget(self, group_id: int, child_id: int) -> Optional[models.Widget]:
        """
        Returns an instance of :obj:`~.models.Widget` class.

        :param group_id: int
            The group ID of the widget
        :param child_id: int
            The child ID of the widget
        :return: Optional[models.Widget]
        """
        params = _make_dict_from_kwargs(groupId=group_id, childId=child_id)
        return self._make_request_for(models.Widget, parameters=params)

    def _post_canvas(self, canvas, random=False):
        json = self._make_json_canvas_from_list(canvas)
        url = f"http://localhost:{self._port}/canvas{'/random' if random else ''}"
        for _ in range(self._max_retry):
            try:
                with self._session.post(url, json=json) as resp:
                    resp.raise_for_status()
                    return models.Coordinates.from_json(resp.content.decode("utf-8"))
            except models.NotLoggedInError:
                continue
        raise models.NotLoggedInError("You are not logged in.")

    def center_point(self, canvas: models.Polys) -> Optional[models.Coordinates]:
        """
        Calulate and returns the center point of a polygon.

        :param canvas: models.Polys
        :return: Optional[models.Coordinates]
        """
        return self._post_canvas(canvas)

    def centre_point(self, canvas: models.Polys) -> Optional[models.Coordinates]:
        """
        Alias for :obj:`~.center_point` class.

        :param canvas: models.Polys
        :return: Optional[models.Coordinates]
        """
        return self.center_point(canvas)

    def random_point(self, canvas: models.Polys) -> Optional[models.Coordinates]:
        """
        Calulate and returns a random point of a polygon.

        :param canvas: models.Polys
        :return: Optional[models.Coordinates]
        """
        return self._post_canvas(canvas, random=True)
