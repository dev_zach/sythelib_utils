============================
Sythelib Utils API Reference
============================

.. autodata:: sythelib_utils.FONTS
    :annotation:

.. autodata:: sythelib_utils.ITEMS
    :annotation:

----

Anti Ban
========
.. automodule:: sythelib_utils.anti_ban
    :members:

----

Colors
======
.. automodule:: sythelib_utils.colors
    :members:

----

Custom Mouse
============
.. automodule:: sythelib_utils.custom_mouse
    :members:

----

Font
====
.. automodule:: sythelib_utils.font
    :members:

----

Inventory
=========
.. automodule:: sythelib_utils.inventory
    :members:

----

Models
======
.. automodule:: sythelib_utils.models
    :members:

----

Plugin
======
.. automodule:: sythelib_utils.plugin_client
    :members:

----

Singleton
=========
.. automodule:: sythelib_utils.singleton
    :members:

----

Tabs
====
.. automodule:: sythelib_utils.tabs
    :members:
    :member-order: groupwise

----

Utils
=====
.. automodule:: sythelib_utils.utils
    :members:

