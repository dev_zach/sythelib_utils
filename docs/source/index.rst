.. Sythelib Utils documentation master file, created by
   sphinx-quickstart on Sun May 10 19:43:22 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

----

Sythelib Utils
==============

SytheLib was created to emulate human-like inputs for Windows. It's able read text on the screen, find and click on
bitmaps (images), or find and click on colors on the screen.

Index
=====

.. toctree::
   :name: mastertoc
   :maxdepth: 2

   sythelib-api-reference
   utils-api-reference
   var-bit-index
   var-player-index


* :ref:`genindex`

History
-------

I originally created this because I wanted to be able to automate some of the processes I did on the web and in online
games. When I searched deeper into it, I learned that some sites use your mouse movements to detect whether you're a
human or a bot (AKA the Google Captcha) as well as the browser you use. With this, you can simply lock it onto a real
chrome browser.

The original SytheLib was created in ChaiScript which wasn't very noob friendly, nor was there much support for it with
outside libraries. I asked the Creator of SytheLib to rewrite the library in Python since there's much more support,
plus it's a very good beginners-friendly language. Python has a plethora of libraries to pick from which could be
implemented into it.

Later on I learned about AHKs in Windows which I asked to be integrated into pylibsythe. It now supports the same
methods AHKs support and more. The only thing I would need to add is debouncing.

Mouse
-----

The mouse appears choppy as it takes random 4-16ms breaks while moving to emulate velocity and acceleration changes in
the mouse movement. It also creates a random mouse path created with gravity and wind functions which was built off the
old Simba color picker.

1% of the time it will miss its target and correct itself. The range it can miss by is randomly
calculated off the speed of the mouse and the total distance the mouse travelled.

Clicking on an object
---------------------

I created functions that are able to randomly click on spots within objects. For example, if you're looking for a
bitmap (image), my functions would find the height and width of the object and randomly click somewhere near the center
of it (to make it appear more human-like).

----

Below you can find a link to the index page for all the items supported by the library.

.. warning::
   This page is quite large and may take some time to load.

.. toctree::
   :name: itemstoc
   :titlesonly:

   items-index
