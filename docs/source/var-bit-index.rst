==================
VarBit Enums Index
==================

A
=

.. hlist::
    :columns: 3

    * ACCOUNT_TYPE
    * AUTOWEED


B
=

.. hlist::
    :columns: 3

    * BANK_REARRANGE_MODE
    * BANK_TAB_EIGHT_COUNT
    * BANK_TAB_FIVE_COUNT
    * BANK_TAB_FOUR_COUNT
    * BANK_TAB_NINE_COUNT
    * BANK_TAB_ONE_COUNT
    * BANK_TAB_SEVEN_COUNT
    * BANK_TAB_SIX_COUNT
    * BANK_TAB_THREE_COUNT
    * BANK_TAB_TWO_COUNT
    * BARROWS_KILLED_AHRIM
    * BARROWS_KILLED_DHAROK
    * BARROWS_KILLED_GUTHAN
    * BARROWS_KILLED_KARIL
    * BARROWS_KILLED_TORAG
    * BARROWS_KILLED_VERAC
    * BARROWS_NPCS_SLAIN
    * BARROWS_REWARD_POTENTIAL
    * BAR_DISPENSER
    * BA_GC
    * BLAST_FURNACE_ADAMANTITE_BAR
    * BLAST_FURNACE_ADAMANTITE_ORE
    * BLAST_FURNACE_BRONZE_BAR
    * BLAST_FURNACE_COAL
    * BLAST_FURNACE_COFFER
    * BLAST_FURNACE_COPPER_ORE
    * BLAST_FURNACE_GOLD_BAR
    * BLAST_FURNACE_GOLD_ORE
    * BLAST_FURNACE_IRON_BAR
    * BLAST_FURNACE_IRON_ORE
    * BLAST_FURNACE_MITHRIL_BAR
    * BLAST_FURNACE_MITHRIL_ORE
    * BLAST_FURNACE_RUNITE_BAR
    * BLAST_FURNACE_RUNITE_ORE
    * BLAST_FURNACE_SILVER_BAR
    * BLAST_FURNACE_SILVER_ORE
    * BLAST_FURNACE_STEEL_BAR
    * BLAST_FURNACE_TIN_ORE
    * BLAST_MINE_ADAMANTITE
    * BLAST_MINE_COAL
    * BLAST_MINE_GOLD
    * BLAST_MINE_MITHRIL
    * BLAST_MINE_RUNITE


C
=

.. hlist::
    :columns: 3

    * CHAT_SCROLLBAR_ON_LEFT
    * CORP_DAMAGE
    * CURRENT_BANK_TAB


D
=

.. hlist::
    :columns: 3

    * DAILY_ARROWS_STATE
    * DAILY_BONEMEAL_STATE
    * DAILY_DYNAMITE_COLLECTED
    * DAILY_ESSENCE_COLLECTED
    * DAILY_FLAX_STATE
    * DAILY_HERB_BOXES_COLLECTED
    * DAILY_RUNES_COLLECTED
    * DAILY_SAND_COLLECTED
    * DAILY_STAVES_COLLECTED
    * DEFENSIVE_CASTING_MODE
    * DIARY_ARDOUGNE_EASY
    * DIARY_ARDOUGNE_ELITE
    * DIARY_ARDOUGNE_HARD
    * DIARY_ARDOUGNE_MEDIUM
    * DIARY_DESERT_EASY
    * DIARY_DESERT_ELITE
    * DIARY_DESERT_HARD
    * DIARY_DESERT_MEDIUM
    * DIARY_FALADOR_EASY
    * DIARY_FALADOR_ELITE
    * DIARY_FALADOR_HARD
    * DIARY_FALADOR_MEDIUM
    * DIARY_FREMENNIK_EASY
    * DIARY_FREMENNIK_ELITE
    * DIARY_FREMENNIK_HARD
    * DIARY_FREMENNIK_MEDIUM
    * DIARY_KANDARIN_EASY
    * DIARY_KANDARIN_ELITE
    * DIARY_KANDARIN_HARD
    * DIARY_KANDARIN_MEDIUM
    * DIARY_KARAMJA_EASY
    * DIARY_KARAMJA_ELITE
    * DIARY_KARAMJA_HARD
    * DIARY_KARAMJA_MEDIUM
    * DIARY_KOUREND_EASY
    * DIARY_KOUREND_ELITE
    * DIARY_KOUREND_HARD
    * DIARY_KOUREND_MEDIUM
    * DIARY_LUMBRIDGE_EASY
    * DIARY_LUMBRIDGE_ELITE
    * DIARY_LUMBRIDGE_HARD
    * DIARY_LUMBRIDGE_MEDIUM
    * DIARY_MORYTANIA_EASY
    * DIARY_MORYTANIA_ELITE
    * DIARY_MORYTANIA_HARD
    * DIARY_MORYTANIA_MEDIUM
    * DIARY_VARROCK_EASY
    * DIARY_VARROCK_ELITE
    * DIARY_VARROCK_HARD
    * DIARY_VARROCK_MEDIUM
    * DIARY_WESTERN_EASY
    * DIARY_WESTERN_ELITE
    * DIARY_WESTERN_HARD
    * DIARY_WESTERN_MEDIUM
    * DIARY_WILDERNESS_EASY
    * DIARY_WILDERNESS_ELITE
    * DIARY_WILDERNESS_HARD
    * DIARY_WILDERNESS_MEDIUM
    * DRIFT_NET_COLLECT


E
=

.. hlist::
    :columns: 3

    * EQUIPPED_WEAPON_TYPE
    * EXPERIENCE_DROP_COLOR
    * EXPERIENCE_TRACKER_COUNTER
    * EXPERIENCE_TRACKER_POSITION
    * EXPERIENCE_TRACKER_PROGRESS_BAR
    * EXPLORER_RING_ALCHS
    * EXPLORER_RING_ALCHTYPE
    * EXPLORER_RING_RUNENERGY
    * EXPLORER_RING_TELEPORTS


F
=

.. hlist::
    :columns: 3

    * FAIRY_RIGH_DIAL_ILJK
    * FAIRY_RING_DIAL_ADCB
    * FAIRY_RING_DIAL_PSRQ
    * FAIR_RING_LAST_DESTINATION
    * FARMING_4771
    * FARMING_4772
    * FARMING_4773
    * FARMING_4774
    * FARMING_4775
    * FARMING_7904
    * FARMING_7905
    * FARMING_7906
    * FARMING_7907
    * FARMING_7908
    * FARMING_7909
    * FARMING_7910
    * FARMING_7911
    * FIRE_PIT_GIANT_MOLE
    * FIRE_PIT_LUMBRIDGE_SWAMP
    * FIRE_PIT_MOS_LE_HARMLESS
    * FISHING_TRAWLER_ACTIVITY
    * FOSSIL_ISLAND_WYVERN_DISABLE


G
=

.. hlist::
    :columns: 3

    * GE_OFFER_CREATION_TYPE
    * GRAPES_4953
    * GRAPES_4954
    * GRAPES_4955
    * GRAPES_4956
    * GRAPES_4957
    * GRAPES_4958
    * GRAPES_4959
    * GRAPES_4960
    * GRAPES_4961
    * GRAPES_4962
    * GRAPES_4963
    * GRAPES_4964


H
=

.. hlist::
    :columns: 3

    * HB_FINISH
    * HB_STARTED
    * HB_TRAIL_31303
    * HB_TRAIL_31306
    * HB_TRAIL_31309
    * HB_TRAIL_31312
    * HB_TRAIL_31315
    * HB_TRAIL_31318
    * HB_TRAIL_31321
    * HB_TRAIL_31324
    * HB_TRAIL_31327
    * HB_TRAIL_31330
    * HB_TRAIL_31333
    * HB_TRAIL_31336
    * HB_TRAIL_31339
    * HB_TRAIL_31342
    * HB_TRAIL_31345
    * HB_TRAIL_31348
    * HB_TRAIL_31351
    * HB_TRAIL_31354
    * HB_TRAIL_31357
    * HB_TRAIL_31360
    * HB_TRAIL_31363
    * HB_TRAIL_31366
    * HB_TRAIL_31369
    * HB_TRAIL_31372


I
=

.. hlist::
    :columns: 3

    * IN_GAME_BA
    * IN_RAID
    * IN_WILDERNESS


K
=

.. hlist::
    :columns: 3

    * KINGDOM_COFFER
    * KINGDOM_FAVOR
    * KOUREND_FAVOR_ARCEUUS
    * KOUREND_FAVOR_HOSIDIUS
    * KOUREND_FAVOR_LOVAKENGJ
    * KOUREND_FAVOR_PISCARILIUS
    * KOUREND_FAVOR_SHAYZIEN


M
=

.. hlist::
    :columns: 3

    * MULTICOMBAT_AREA


N
=

.. hlist::
    :columns: 3

    * NMZ_ABSORPTION
    * NMZ_POINTS
    * NORTH_NET_CATCH_COUNT
    * NORTH_NET_STATUS


O
=

.. hlist::
    :columns: 3

    * OXYGEN_LEVEL


P
=

.. hlist::
    :columns: 3

    * PERSONAL_POINTS
    * PRAYER_AUGURY
    * PRAYER_BURST_OF_STRENGTH
    * PRAYER_CHIVALRY
    * PRAYER_CLARITY_OF_THOUGHT
    * PRAYER_EAGLE_EYE
    * PRAYER_HAWK_EYE
    * PRAYER_IMPROVED_REFLEXES
    * PRAYER_INCREDIBLE_REFLEXES
    * PRAYER_MYSTIC_LORE
    * PRAYER_MYSTIC_MIGHT
    * PRAYER_MYSTIC_WILL
    * PRAYER_PIETY
    * PRAYER_PRESERVE
    * PRAYER_PROTECT_FROM_MAGIC
    * PRAYER_PROTECT_FROM_MELEE
    * PRAYER_PROTECT_FROM_MISSILES
    * PRAYER_PROTECT_ITEM
    * PRAYER_RAPID_HEAL
    * PRAYER_RAPID_RESTORE
    * PRAYER_REDEMPTION
    * PRAYER_RETRIBUTION
    * PRAYER_RIGOUR
    * PRAYER_ROCK_SKIN
    * PRAYER_SHARP_EYE
    * PRAYER_SMITE
    * PRAYER_STEEL_SKIN
    * PRAYER_SUPERHUMAN_STRENGTH
    * PRAYER_THICK_SKIN
    * PRAYER_ULTIMATE_STRENGTH
    * PVP_SPEC_ORB
    * PYRAMID_PLUNDER_ROOM
    * PYRAMID_PLUNDER_ROOM_LOCATION
    * PYRAMID_PLUNDER_THIEVING_LEVEL
    * PYRAMID_PLUNDER_TIMER


Q
=

.. hlist::
    :columns: 3

    * QUEST_TAB
    * QUEST_THE_HAND_IN_THE_SAND
    * QUICK_PRAYER


R
=

.. hlist::
    :columns: 3

    * RAID_PARTY_SIZE
    * RAID_STATE
    * RUNE_POUCH_AMOUNT1
    * RUNE_POUCH_AMOUNT2
    * RUNE_POUCH_AMOUNT3
    * RUNE_POUCH_RUNE1
    * RUNE_POUCH_RUNE2
    * RUNE_POUCH_RUNE3
    * RUN_SLOWED_DEPLETION_ACTIVE


S
=

.. hlist::
    :columns: 3

    * SACK_NUMBER
    * SACK_UPGRADED
    * SIDE_PANELS
    * SOUTH_NET_CATCH_COUNT
    * SOUTH_NET_STATUS
    * SPICY_STEW_BROWN_SPICES
    * SPICY_STEW_ORANGE_SPICES
    * SPICY_STEW_RED_SPICES
    * SPICY_STEW_YELLOW_SPICES
    * SUPERIOR_ENABLED


T
=

.. hlist::
    :columns: 3

    * THEATRE_OF_BLOOD
    * TITHE_FARM_POINTS
    * TITHE_FARM_SACK_AMOUNT
    * TITHE_FARM_SACK_ICON
    * TOTAL_POINTS
    * TRANSPARENT_CHATBOX
    * TWISTED_LEAGUE_RELIC_1
    * TWISTED_LEAGUE_RELIC_2
    * TWISTED_LEAGUE_RELIC_3
    * TWISTED_LEAGUE_RELIC_4
    * TWISTED_LEAGUE_RELIC_5


V
=

.. hlist::
    :columns: 3

    * VENGEANCE_ACTIVE
    * VENGEANCE_COOLDOWN


W
=

.. hlist::
    :columns: 3

    * WINTERTODT_TIMER
    * WORLDHOPPER_FAVROITE_1
    * WORLDHOPPER_FAVROITE_2

